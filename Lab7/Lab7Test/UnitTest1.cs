﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab7;

namespace Lab7Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CreateHeap()
        {
            // Create a heap from an array. Heap constructor automatically moves values around
            // to keep heap constraint
            int[] testArray = new int[] { 22, 85, 12, 1, 225, 154, 100, 122, 80, 10 };
            int[] expectedHeap = new int[] { 225, 22, 154, 122, 85, 12, 100, 1, 80, 10 };
            Heap<int> testHeap = new Heap<int>(testArray);

            Assert.AreEqual(expectedHeap, testHeap);
        }
    }
}
