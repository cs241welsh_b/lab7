﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    public class Heap<Type> where Type:IComparable
    {
        public Heap(Type[] someArray)
        {
            theArray = someArray;
            
            heapify();

            // To reference the first item
            // this[1] = this[2];
        }

        // We want indexes that start at 1 rather than 0
        public ref Type this[int index]
        {
            get
            {
                return ref theArray[index - 1];
            }

        }
        public void exchange(int firstIndex, int secondIndex)
        {
            Type temp = theArray[firstIndex];
            theArray[firstIndex] = theArray[secondIndex];
            theArray[secondIndex] = temp;
        }

        public void heapify()
        {
             // Convert an array into a heap
             // Start at the halfway point, and sink each key
             // decrementing by 1 each time
             for(int i = theArray.Length / 2; i >=1; i--)
            {
                sink(i, theArray.Length);
            }
        }

        public void sink(int index, int arrayLength)
        {
            // Sink a key until the heap order is restored
            // While the children of index isn't out of bounds...
            while(2 * index <= arrayLength)
            {
                // The first child of index
                int j = index * 2;

                // If j isn't the end AND the value at j is less than the value at j+1,
                // increment j to represent the second child of index
                if (j < arrayLength && less(j, j + 2)) j++;

                // If the value at index is greater than the value at j, leave the loop
                if (!less(index, j)) break;

                // Exchange the values at index and j, and loop again this time with j as the index
                exchange(index, j);
                index = j;
            }
        }

        public void swim(int index)
        {
            // The parent of the key at index can be found with index/2
            // While we aren't at the root, and this item is greater than its parent...
            while(index > 1 && less(index/2, index))
            {
                exchange(index / 2, index);
                index = index / 2;
            }
        }

        public void sort()
        {
            // Sort the heap in ascending order starting at the last terminal node
            int n = theArray.Length;

            // While we arent at the root...
            while(n > 1)
            {
                // Exchange the value at root with the last node
                exchange(1, n);

                // Sink the new root to repair the heap status
                sink(1, n);
            }
        }

        private bool less(int i, int j)
        {
            return theArray[i].CompareTo(theArray[j - 1]) < 0;
        }

        Type[] theArray;
       
    }
}
